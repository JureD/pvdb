# Private video Database

PVDB je spletna aplikacija napisana v framework-u Flas za indeksiranje kolekcije filmov.

Filme, ki jih dodamo v mapo `./pvdb/static/video-library` avtomatsko indeksira in jih prikaže v zbirki, kjer jim nato lahko dodamo: 
* igralce ki se v tem filmu pojavijo,
* tag-e, ki klasificirajo ta video
* osebno oceno filma
* komentar (privzeta vrednost je naslov filma)

Filme lahko kasneje iščemo po igralcih, tag-ih ali oceni.