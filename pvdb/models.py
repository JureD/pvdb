from pvdb import db
from datetime import datetime

# DB classes - MODELS
actresses_association_table = db.Table('actresses_association_table',
    db.Column('video_id', db.Integer, db.ForeignKey('video.id')),
    db.Column('actress_id', db.Integer, db.ForeignKey('actress.id'))
)

tags_association_table = db.Table('tags_association_table',
    db.Column('video_id', db.Integer, db.ForeignKey('video.id')),
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.id'))
)

class Videos(db.Model):
    __tablename__ = 'video'
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String, unique=True, nullable=False)
    length = db.Column(db.Integer)
    rating = db.Column(db.Integer)
    notes = db.Column(db.String)
    date_added = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return f"Video {self.id}"

class Actresses(db.Model):
    __tablename__ = 'actress'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True)
    count = db.Column(db.Integer)
    video = db.relationship('Videos', secondary=actresses_association_table, backref=db.backref('starring'), lazy=True)


    def __repr__(self):
        return f"Actress {self.id}"

class Tags(db.Model):
    __tablename__ = 'tag'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True)
    count = db.Column(db.Integer)
    video = db.relationship('Videos', secondary=tags_association_table, backref=db.backref('tagged'), lazy=True)

    def __repr__(self):
        return f"Tags {self.id}"