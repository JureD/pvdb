from flask import render_template, url_for, request, redirect
from pvdb import app
from pvdb.models import *
from pvdb.helper import import_videos



# Routes
@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        # Getting selector values
        actress_selected = request.form.get('actress_select')
        # Getting checkbox values
        tags_selected = []

        for tag in Tags.query.all():
            value = request.form.get(str(tag.id))
            if value == 'on':
                tags_selected.append(tag.id)

        videos = Videos.query

        if request.form.get('actres_find') == 'on':
                videos = videos.filter(Videos.starring.any(id=actress_selected))

        for tag_id in tags_selected:
            videos = videos.filter(Videos.tagged.any(id=tag_id))
        
        return render_template('/search_results.html', videos=videos)
    else:
        videos = Videos.query.all()
        videos = Videos.query.all()
        actresses = Actresses.query.all()
        tags = Tags.query.all()
         
        return render_template('index.html', videos=videos, actresses=actresses, tags=tags)

@app.route('/edit', methods=['POST', 'GET'])
def edit():
    if request.method == 'POST':
        # Getting value from input for new actress
        added_actress = request.form.get('new-actress')
        # Getting value from input for new tag
        added_tag = request.form.get('new-tag')
        # Getting value from checkbox to import new videos
        import_new = request.form.get('import_new')
        
        if added_actress != '':
            new_actress = Actresses(name=added_actress)
            db.session.add(new_actress)
        if added_tag != '':
            new_tag = Tags(name=added_tag)
            db.session.add(new_tag)
        db.session.commit()
        if import_new != '':
            import_videos()

        return redirect('/edit')
    else:
        actresses = Actresses.query.all()
        tags = Tags.query.all()
        return render_template('edit.html', actresses=actresses, tags=tags)

@app.route('/edit_video/<int:id>', methods=['GET', 'POST'])
def edit_video(id):
    video = Videos.query.get_or_404(id)

    if request.method == 'POST':
        # Getting selector values
        actress_selected = request.form.get('actress_select')
        # Getting checkbox values
        rating_selected = request.form.get('rating-select')

        for tag in Tags.query.all():
            value = request.form.get(str(tag.id))
            if value == 'on':
                video.tagged.append(Tags.query.get_or_404(tag.id))

        video.starring.append(Actresses.query.get_or_404(actress_selected))
        video.rating = rating_selected
        db.session.commit()
        
        return redirect('/')
    
    else:
        actresses = Actresses.query.all()
        tags = Tags.query.all()

        return render_template('edit_video.html', video=video, tags=tags, actresses=actresses)