from pvdb import app
from pvdb.models import *
import os
from pymediainfo import MediaInfo
from flask import url_for

def import_videos():
    no_of_videos = Videos.query.count()

    files_location = './pvdb/static/video-libraray/'

    for one_file in os.listdir(files_location):
        if one_file[:4] == 'pdb_':
            pass
        else:
            no_of_videos += 1
            oldext = os.path.splitext(one_file)[1]
            new_filename = 'pdb_' + str(no_of_videos) + oldext
            video_length = MediaInfo.parse(files_location + one_file).tracks[0].duration
            new_video = Videos(filename=new_filename, notes=one_file, length=video_length)
            db.session.add(new_video)
            os.rename(files_location + one_file, files_location + new_filename)
    
    db.session.commit()